Rostercraft = {}

Rostercraft.commands = {
	dump =
		function(name)
			Rostercraft:DumpCharacter(name)
		end,
	list =
		function()
			Rostercraft:ListCharacters()
		end,
	purge =
		function(arg)
			Rostercraft:PurgeDatas(arg)
		end,
	save =
		function()
			Rostercraft:GetEquipment()
			Rostercraft:GetAverageItemLevel()
			Rostercraft:SaveCharacter()
		end,
}

local UnitSlots={"Head","Neck","Shoulder","Shirt","Chest","Waist","Legs","Feet","Wrist","Hands","Finger0","Finger1","Trinket0","Trinket1","Back","MainHand","SecondaryHand","Ranged","Tabard"}

function Rostercraft:GetGuild()
	if (IsInGuild()) then
		local guildName = GetGuildInfo("player")
		return guildName
	else
		return "None"
	end
end

function Rostercraft:GetPrimarySpec()
	local inspect, pet = false, false
	local talentGroup = GetActiveTalentGroup(inspect, pet)
	local _,_,_,name = GetTalentTabInfo(talentGroup, nil, unit == "pet")
	local talents = {}
	
	_,_,talents[0] = GetTalentTabInfo(1, nil, unit == "pet")
	_,_,talents[1] = GetTalentTabInfo(2, nil, unit == "pet")
	_,_,talents[2] = GetTalentTabInfo(3, nil, unit == "pet")
	
	local talentString = talents[0] .. "/" .. talents[1] .. "/" .. talents[2]
	
	return name, talentString
end

function Rostercraft:GetSecondarySpec()
	local talents = {}
	local names = {}
	
	_,_,talents[0], names[0] = GetTalentTabInfo(1, nil, unit == "pet", 2)
	_,_,talents[1], names[1] = GetTalentTabInfo(2, nil, unit == "pet", 2)
	_,_,talents[2], names[2] = GetTalentTabInfo(3, nil, unit == "pet", 2)
	
	local key, max = 0, talents[0]
	for k, v in pairs(talents) do 
		if (v > max) then
			key, max = k, v
		end
	end

	local talentString = talents[0] .. "/" .. talents[1] .. "/" .. talents[2]
	
	return names[key], talentString
end

function Rostercraft:GetEquipment()
	for index, slot in pairs(UnitSlots) do
		local itemId = GetInventoryItemID("player", index)
		
		if (itemId ~= nil) then
			local itemName,_,_,iLvl = GetItemInfo(itemId)
		
			self.datas.gear[slot] = {}
			self.datas.gear[slot]["ID"] = itemId
			self.datas.gear[slot]["name"] = itemName
			self.datas.gear[slot]["ilvl"] = iLvl
		end
	end
end

function Rostercraft:GetAverageItemLevel()
	local sum, num = 0, 0
	for slot, item in pairs(self.datas.gear) do
		if (slot ~= "Tabard") then
			num = num + 1
			sum = sum + item.ilvl
		end
	end
	if (num == 0) then
		self.datas.iLvl = 0
	else
		self.datas.iLvl = sum / num
	end
end

function Rostercraft:PrintUsage()
	print("Usage : /rostercraft cmd [args]")
	print("You can also use /rc or /rcraft instead of /rostercraft")
	print("/rc dump [character] -- Used to dump datas on a given character.")
	print("/rc list -- Used to list characters.")
	print("/rc purge [all/realm/character] -- Used to delete profiles")
	print("/rc save -- Used to save current character or update its profile if already existing.")
end

function Rostercraft:DumpCharacter(args)
	if (args and args[0]) then
		if (RosterDB and RosterDB[self.datas.realm] and RosterDB[self.datas.realm][args[0]]) then
			local character = RosterDB[self.datas.realm][args[0]]
			print("Name : " .. character.name)
			print("Class : " .. character.class)
			print("Race : " .. character.race)
			print("Level : " .. character.level)
			print("Guild : " .. character.guild)
			if (character.specialization.active) then
				print("Active Specialization : " .. character.specialization.active .. " (" .. character.talents.active .. ")")
			else
				print("Active Specialization : Unknown (" .. character.talents.active .. ")")
			end
			if (character.specialization.inactive) then
				print("Inactive Specialization : " .. character.specialization.inactive .. " (" .. character.talents.inactive .. ")")
			else
				print("Inactive Specialization : Unknown (" .. character.talents.active .. ")")
			end
			print("Equipped iLvl : " .. character.iLvl)
		else
			print("Character not found !")
		end
	else
		self:PrintUsage()
	end
end

function Rostercraft:ListCharacters()
	if (RosterDB and RosterDB[self.datas.realm]) then
		local c = 0
		for i,v in pairs(RosterDB[self.datas.realm]) do
			print(i .. " : " .. v.race .. " " .. v.class .. " level " .. v.level)
			c = c + 1
		end
		if (c == 0) then
			print("No characters saved for this realm.")
		end
	else
		print("No characters saved for this realm.")
	end
end

function Rostercraft:PurgeDatas(arg)
	if (arg == nil or string.lower(arg[0]) == "character") then
		if (RosterDB and RosterDB[self.datas.realm]) then
			RosterDB[self.datas.realm][self.datas.name] = nil
		end
	elseif (string.lower(arg[0]) == "all") then
		RosterDB = nil
	elseif (string.lower(arg[0]) == "realm") then
		if (RosterDB) then
			RosterDB[self.datas.realm] = nil
		end
	else
		self:PrintUsage()
	end
end

function Rostercraft:SaveCharacter()
	if (RosterDB == nil) then
		RosterDB = {}
	end
	if (RosterDB[self.datas.realm] == nil) then
		RosterDB[self.datas.realm] = {}
	end
	RosterDB[self.datas.realm][self.datas.name] = {}
	RosterDB[self.datas.realm][self.datas.name] = self.datas
	print(self.datas.name .. " saved !")
end

function Rostercraft:OnInitialize()
	self.datas = {}
	self.datas.name = UnitName("player")
	_, self.datas.class = UnitClass("player")
	_, self.datas.race = UnitRace("player")
	self.datas.realm = GetRealmName()
	self.datas.level = UnitLevel("player")
	self.datas.guild = self:GetGuild()
	self.datas.specialization = {}
	self.datas.talents = {}
	self.datas.specialization.active, self.datas.talents.active = self:GetPrimarySpec()
	self.datas.specialization.inactive, self.datas.talents.inactive = self:GetSecondarySpec()
	self.datas.gear = {}
	self.datas.iLvl = 0
	SLASH_ROSTERCRAFT1 = "/rc"
	SLASH_ROSTERCRAFT2 = "/rostercraft"
	SLASH_ROSTERCRAFT3 = "/rcraft"
	SlashCmdList["ROSTERCRAFT"] = function(args)
		local options = {}
		local searchResult = { string.match(args,"^(%S*)%s*(.-)$") }
		for i,v in pairs(searchResult) do
			if (v ~= nil and v ~= "") then
				if (i == 1) then
					options[i - 1] = string.lower(v)
				else
					options[i - 1] = v
				end
			end
		end
		if (options and options[0]) then
			local argcase = options[0]
			table.remove(options, 0);
			self.commands[argcase](options)
		else
			self:PrintUsage()
		end
	end
end

local frame = CreateFrame("Frame")
frame:RegisterEvent("ADDON_LOADED")
frame:SetScript("OnEvent", function(self, event, ...)
	if(event == "ADDON_LOADED") then
		if(select(1, ...) == "Rostercraft") then
			Rostercraft:OnInitialize()
			self:UnregisterEvent("ADDON_LOADED")
		end
	end
end)